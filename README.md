# tp6+workerman-task

#### 介绍
tp6使用的worker写的异步任务模型

#### 安装教程

1.  composer require maowenke/tp6-workerman-task dev-master

#### 使用说明
配置config下面的worker_task.php


然后命令行启动 php think worker:task start
监听端口

在代码中直接调用AsynchronousTaskProducer::$address=你的监听地址

然后调用AsynchronousTaskProducer::send($data,$Processing)
$data为参数数组,$Processing为处理数据的类或者类名字符串
windows上为单进程,liunx上课设置成多进程
处理进程的类必须实现public 的 fire方法
<pre>
        public function index()
    {
        AsynchronousTaskProducer::$address = '127.0.0.1:19345';
        for ($i=0;$i<10;$i++){
            $gateway_buffer = [
                'id'=>10-$i,
                'name'=>17
            ];
            $obj = "\app\index\buss\Ces";
            AsynchronousTaskProducer::send($gateway_buffer,$obj);
        }
        echo '成功';exit;
    }
</pre>
#### 参与贡献

1.  沐雨轩
2.  thinkphp

